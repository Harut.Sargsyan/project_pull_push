


<?php





/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use borales\extensions\phoneInput\PhoneInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $model common\models\User */
?>


<div class="user-settings-bgimg">
    <div class="user-settings-tab">
        <ul class="nav nav-tabs user-settings">
            <li class="<?= $tabs == 'tab1' ? 'active' : '' ?>"><a data-toggle="tab" href="#tab1"><?= Yii::t('main','Profile')?></a></li>
            <li class="<?= $tabs == 'tab2' ? 'active' : '' ?>"><a data-toggle="tab" href="#tab2"><?= Yii::t('main','Education')?></a></li>
            <li class="<?= $tabs == 'tab3' ? 'active' : '' ?>"><a data-toggle="tab" href="#tab3"><?= Yii::t('main','Preferences')?></a></li>
        </ul>
    </div>
</div>
<div class="tab-content">
    <div id="tab1" class="tab-pane fade <?= $tabs == 'tab1' ? 'in active' : '' ?>">

            <div class="row add-notes-form user-setting-form">
            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

                <div class="col-md-4 col-md-offset-2" style="text-align: left!important">
                    <div class="form-group">
                        <input id="tab1" name="tab1" type="hidden" value="tab1">
                    </div>


                    <div class="form-group">
                        <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="form-group">
                        <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="form-group">
                        <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
                    </div>

                    <div class="dropdown">
                        <?= $form->field($model, 'currency_id')->dropDownList($currency, ['class' => 'currency-select form-group add-currency-list'])->label('Currency') ?>
                    </div>
                </div>



                <div class="col-md-4" style="text-align: left!important"><div class="form-group"></div>
                    <label class="img_label" for="<?= $model['img']; ?>"><?= Yii::t('main','Upload profile image')?></label>
                    <div style="position:relative;" class="input-wrapper">

                        <a class='btn btn-primary' href='javascript:;'>
                            Choose Image
                            <input type="file" name="User[img]"
                                   style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=1);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;'
                                   size="40"  onchange='$("#upload-file-info").html($(this).val());'>
                        </a>
                        &nbsp;
                        <span class='label label-info' id="upload-file-info"></span>

                    </div>

                    <?php if(isset($model['img']) && !empty($model['img'])) { ?>
                        <span><?= Html::img("/uploads/user/" . $model['img'], ['class' => 'profile_img']) ?></span>

                    <?php } ?>


                    <?php if (isset($model->person_type) && ($model->person_type == 'professor')) {?>
                        <label class="img_label" for="<?= $model['img']; ?>"><?= Yii::t('main','Upload Your CV')?></label>
                        <div style="position:relative;" class="input-wrapper">

                            <a class='btn btn-primary' href='javascript:;'>
                                Choose CV
                                <input type="file" name="User[cv]"
                                       style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=1);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;'

                                       size="40"
                                       onchange='$("#upload-file-info-cv").html($(this).val());'>
                            </a>
                            &nbsp;
                            <span class='label label-info' id="upload-file-info-cv"></span>
                        </div>

                        <?php if(isset($model['cv']) && !empty($model['cv'])) { ?>
                            <span><?= Html::a( '<i class="far fa-file-pdf" style="font-size: 36px; margin-top: 15px;"></i>', "/uploads/user/" . $model['cv'], ['class' => 'profile_cv']) ?></span>

                        <?php } ?>


                    <?php } ?>
                    <div class="form-group row">
                        <?= Html::submitButton('Submit', ['class' => 'btn  btn-success']) ?>
                    </div>
                </div>


            <?php ActiveForm::end(); ?>
        </div>
        <!--        </div>-->

    </div>



    <div id="tab2" class="tab-pane fade <?= $tabs == 'tab2' ? 'in active' : '' ?>">
        <div class="row add-notes-form user-setting-form">

            <?php $form = ActiveForm::begin(); ?>
            <input id="tab2" name="tab1" type="hidden" value="tab2">
            <fieldset>
                <div class="repeater-custom-show-hide">
                    <div data-repeater-list="User">

                        <?php if(isset($model1) && !empty($model1)){?>
                            <?php foreach($model1 as $items): ?>


                                <div data-repeater-item="">
                                    <div class="form-group">

                                            <div class="col-md-8 col-md-offset-2">
                                                 <span data-repeater-delete="" class="btn btn-danger btn-sm" style="float: right; margin-top: -13px">
                                                      <span class="glyphicon glyphicon-remove"></span>
                                                 </span>
                                                <hr class="add_form_hr">
                                            </div>
                                            <div class="col-md-4 col-md-offset-2" style="text-align: left!important">


                                                <div class="dropdown">
                                                    <?= $form->field($items, 'university_id')->dropDownList([$items['university']['id'] => $items['university']['name']],
                                                        ['class' => 'university-select add-universrsity-list', 'id' => ''])->label('University') ?>
                                                </div>

                                                <div class="dropdown">
                                                    <?= $form->field($items, 'faculty_id')->dropDownList([$items['faculty']['id'] => $items['faculty']['name']],
                                                        ['class' => 'faculty-select add-universrsity-list', 'id' => ''])->label('Faculty') ?>
                                                </div>

                                            </div>


                                            <div class="col-md-4" style="text-align: left!important">
                                                <div class="dropdown">
                                                    <?= $form->field($items, 'department_id')->dropDownList([$items['department']['id'] => $items['department']['name']],

                                                        ['class' => 'department-select add-universrsity-list', 'id' => ''])->label('Department') ?>

                                                </div>

                                                <div class="dropdown">
                                                    <?= $form->field($items, 'profession_id')->dropDownList([$items['profession']['id'] => $items['profession']['name']],

                                                        ['class' => 'profession-select add-universrsity-list', 'id' => ''])->label('Profession') ?>

                                                </div>

                                                <label class="control-label" for="year_authored"><?= Yii::t('main','Education')?></label>
                                                <div style="display: flex">
                                                    <?php $already_selected_value = 2008;
                                                    $earliest_year = 1950;


                                                    print '<select class="study_start" name="User[study_start]">';
                                                    foreach (range(date('Y'), $earliest_year) as $x) {
                                                        print '<option value="' .  $x . '"' . (isset($items['study_start']) && $items['study_start']==$x ? ' selected="selected"' : '') . '>' . $x . '</option>';
                                                    }
                                                    print '</select>'; ?>


                                                    <span>&nbsp;__&nbsp;</span>
                                                    <?php $already_selected_value = 2008;
                                                    $earliest_year = 1950;


                                                    print '<select class="study_start" name="User[study_end]">';
                                                    foreach (range(date('Y'), $earliest_year) as $x) {
                                                        print '<option value="' . $x . '"' . (isset($items['study_end']) && $items['study_end']==$x ? ' selected="selected"' : '') . '>' . $x . '</option>';
                                                    }
                                                    print '</select>'; ?>

                                                </div>
                                            </div>

                                    </div>
                                </div>
                            <?php endforeach;?>
                        <?php }else{ ?>
                            <!--                                                        --><?//= var_dump(55555);die; ?>
                            <div data-repeater-item="">
                                <div class="form-group">

                                        <div class="col-md-8 col-md-offset-2">
                                                 <span data-repeater-delete="" class="btn btn-danger btn-sm" style="float: right; margin-top: -13px">
                                                      <span class="glyphicon glyphicon-remove"></span>
                                                 </span>
                                            <hr class="add_form_hr">
                                        </div>
                                        <div class="col-md-4 col-md-offset-2" style="text-align: left!important">




                                            <div class="dropdown">
                                                <?= $form->field($model2, 'university_id')->dropDownList([], ['class' => 'university-select add-universrsity-list', 'id' => ''])->label('Universities') ?>

                                            </div>
                                            <div class="dropdown">
                                                <?= $form->field($model2, 'faculty_id')->dropDownList([], ['class' => 'faculty-select add-universrsity-list', 'id' => ''])->label('Faculty') ?>

                                            </div>
                                        </div>

                                        <div class="col-md-4" style="text-align: left!important">
                                            <div class="dropdown">
                                                <?= $form->field($model2, 'department_id')->dropDownList([], ['class' => 'department-select add-universrsity-list', 'id' => ''])->label('Department') ?>

                                            </div>

                                            <div class="dropdown">
                                                <?= $form->field($model2, 'profession_id')->dropDownList([], ['class' => 'profession-select add-universrsity-list', 'id' => ''])->label('Profession') ?>

                                            </div>


                                            <label class="control-label" for="year_authored">Education</label>
                                            <div style="display: flex">
                                                <?php $already_selected_value = 2008;
                                                $earliest_year = 1950;

                                                print '<select class="study_start" name="User[study_start]">';
                                                foreach (range(date('Y'), $earliest_year) as $x) {
                                                    print '<option value="' . $x . '"' . ($x === $already_selected_value ? ' selected="selected"' : '') . '>' . $x . '</option>';
                                                }
                                                print '</select>'; ?>
                                                <span>&nbsp;__&nbsp;</span>
                                                <?php $already_selected_value = 2018;
                                                $earliest_year = 1950;

                                                print '<select class="study_start" name="User[study_end]">';
                                                foreach (range(date('Y'), $earliest_year) as $x) {
                                                    print '<option value="' . $x . '"' . ($x === $already_selected_value ? ' selected="selected"' : '') . '>' . $x . '</option>';
                                                }
                                                print '</select>'; ?>
                                            </div>
                                        </div>
                                </div>
                            </div>

                        <?php  }?>
                    </div>

                        <div class="form-group">
                            <div class="col-md-offset-2 col-sm-8">
                                <hr class="add_form_hr">
                                <span data-repeater-create="" class="btn btn-info btn-md">
                                       <span class="glyphicon glyphicon-plus"></span> Add
                                    </span>
                            </div>
                        </div>

                </div>

            </fieldset>
            <div class="form-group" style="text-align: center">
                <?= Html::submitButton('Submit', ['class' => 'btn  btn-success']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

    </div>



    <div id="tab3" class="tab-pane fade <?= $tabs == 'tab3' ? 'in active' : '' ?>">
        <div class="row add-notes-form user-setting-form">
            <?php $form = ActiveForm::begin(); ?>




                <div class="col-md-4 col-md-offset-4">
                    <label class="cat_label"><?= Yii::t('main','Select category')?></label>
                    <div style="clear: both"></div>



                    <?php foreach ($documrnt_categories as $item): ?>

                        <?= $form->field($item, 'id[]', ['options' => ['class' => 'xform-group']])->checkbox([

                            ' checked' => in_array($item->id, $rows),
                            'uncheck' => null,
                            'class' => 'chk',
                            'id' => $item->id,
                            'label' => '<span>' . $item->name . '</span>',
                            'labelOptions' => ['class' => 'chk full-width cat_label'],
                            'template' => '{input}{beginLabel}{labelTitle}{endLabel}{error}',
                            'value' => $item->id,

                        ]); ?>
                    <?php endforeach; ?>
                </div>

                <div style="clear: both"></div>



                <div class="form-group-cat">
                    <?= Html::submitButton('Submit', ['class' => 'btn btn-success']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>









